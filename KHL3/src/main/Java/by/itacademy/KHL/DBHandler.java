package by.itacademy.KHL;

import by.itacademy.KHL.domain.Club;
import by.itacademy.KHL.domain.Player;
import by.itacademy.KHL.domain.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBHandler {
    private DataSource ds = DataSource.getInstance();

    public List<Player> load() throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            List<Player> players = new ArrayList<>();
            connection = ds.getConnection();
            pstmt = connection.prepareStatement(
                    "select * from player");
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Player player = new Player.Builder()
                        .setName(rs.getString(2))
                        .setCitizenship(rs.getString(3))
                        .setBirthDay(rs.getString(4))
                        .setSeasons(rs.getInt(5))
                        .setTeam(Club.valueOf(rs.getString(6)))
                        .setRole(Role.valueOf(rs.getString(7)))
                        .setMatches(rs.getInt(8))
                        .effectiveness(rs.getInt(9))
                        .build();
                players.add(player);
            }
            return players;
        } catch (SQLException e) {
            throw new Exception(e);
        } finally {
            try {
                if (rs != null) rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (pstmt != null) pstmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (connection != null) connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
