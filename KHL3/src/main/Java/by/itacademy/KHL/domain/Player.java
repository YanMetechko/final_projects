package by.itacademy.KHL.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.IllegalFormatException;
import java.util.Objects;

public class Player implements Serializable {
    private String name;
    private String citizenship;
    private LocalDate birthDay;
    private int seasons;
    private Club team;
    protected Role role;
    private int matches;
    private int effectiveness;


    public static class Builder {
        private Player player = new Player();

        public Builder setName(String name) {
            player.name = name;
            return this;
        }

        public Builder setCitizenship(String citizenship) {
            player.citizenship = citizenship;
            return this;
        }

        public Builder setBirthDay(String ddMMyyyy) {
            try {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
                LocalDate date = LocalDate.parse(ddMMyyyy, formatter);
                player.birthDay = date;
            } catch (IllegalFormatException e) {
                e.getMessage();
            }
            return this;
        }

        public Builder setSeasons(int seasons) {
            player.seasons = seasons;
            return this;
        }

        public Builder setTeam(Club club) {
            player.team = club;
            return this;
        }

        public Builder setRole(Role role) {
            player.role = role;
            return this;
        }

        public Builder setMatches(int matches) {
            player.matches = matches;
            return this;
        }

        public Builder effectiveness(int effectiveness) {
            player.effectiveness = effectiveness;
            return this;
        }

        public Player build() {
            return player;
        }
    }

    public String getName() {
        return name;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }


    public int getSeasons() {
        return seasons;
    }

    public Club getTeam() {
        return team;
    }

    public Role getRole() {
        return role;
    }

    public int getMatches() {
        return matches;
    }

    public int getEffectiveness() {
        return effectiveness;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(name).append(", ").append(citizenship).append(", ");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String birthDate = formatter.format(birthDay);
        stringBuilder.append(birthDate).append(", ").append(team).append(", ").append(role);
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Player)) return false;
        Player player = (Player) o;
        return seasons == player.seasons &&
                matches == player.matches &&
                effectiveness == player.effectiveness &&
                name.equals(player.name) &&
                citizenship.equals(player.citizenship) &&
                birthDay.equals(player.birthDay) &&
                team == player.team &&
                role == player.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, citizenship, birthDay, seasons, team, role, matches, effectiveness);
    }
}