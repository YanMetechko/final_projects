<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Yan Metechko
  Date: 13.01.2019
  Time: 23:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sorted by age roster of <%=request.getParameter("club")%></title>
</head>
<body>


<table>
    <tr>
        <td>
            Player
        </td>
        <td>
            Citizenship
        </td>
        <td>
            Birth Day
        </td>
        <td>
            seasons played in the League
        </td>
        <td>
            Role
        </td>
        <td>
            Quantity of matches played current season
        </td>
        <td>
           Stats
        </td>
    </tr>
    <c:forEach var="player" items="${roster}">
        <tr>
            <td>
                    ${player.name}
            </td>
            <td>
                    ${player.citizenship}
            </td>
            <td>
                    ${player.birthDay}
            </td>
            <td>
                    ${player.seasons}
            </td>
            <td>
                    ${player.role}
            </td>
            <td>
                    ${player.matches}
            </td>
            <td>
                    ${player.effectiveness}
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
