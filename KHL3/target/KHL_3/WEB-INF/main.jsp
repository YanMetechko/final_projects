<%--
  Created by IntelliJ IDEA.
  User: Yan Metechko
  Date: 14.01.2019
  Time: 1:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>KHL League</title>
</head>
<h1>KHL League</h1>
<h2>Available options:</h2>
<body>
<form action="/sortedByAgeClubRoster.jsp" method="GET">
    <h3>Roster ranking by age</h3>
    <p>Insert the club..</p>
    <input type="text" name="club"/>
    <input type="submit" value="ok" name="submit"/>
</form>
<form action="/sortedBySeasonsClubRoster.jsp" method="GET">
    <h3>Roster ranking by seasons played in the League</h3>
    <p>Insert the club..</p>
    <input type="text" name="seasons"/>
    <input type="submit" value="ok" name="submit"/>
</form>

<form action="/search.jsp" method="GET">
    <h3>Separate searching queries</h3>
    <p>Insert a player's name..</p>
    <input type="text" name="name"/>
    <input type="submit" value="ok" name="submit"/>
    <p>Insert a player's role..</p>
    <input type="text" name="club"/>
    <input type="submit" value="ok" name="submit"/>
</form>
<h3>League stats</h3>
<form action="/averageGamesPlayedList.jsp" method="POST">
    <p><input type="submit" value="Show players who played average quantity of games in the League"/></p>
</form>
<form action="/playersStatsHighlightsList.jsp" method="POST">
    <p><input type="submit" value="Show League stats highlights"/></p>
</form>
<form action="/citizenshipStatsOutlet.jsp" method="POST">
    <p><input type="submit" value="Show League citizenship structure"/></p>
</form>
</body>
</html>
