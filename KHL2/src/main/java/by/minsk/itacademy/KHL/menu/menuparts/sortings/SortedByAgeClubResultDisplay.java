package by.minsk.itacademy.KHL.menu.menuparts.sortings;

import by.minsk.itacademy.KHL.domain.Club;
import by.minsk.itacademy.KHL.domain.League;
import by.minsk.itacademy.KHL.domain.Player;
import by.minsk.itacademy.KHL.menu.MenuItem;
import by.minsk.itacademy.KHL.menu.menuparts.ResultDisplay;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class SortedByAgeClubResultDisplay implements ResultDisplay {

    private List<Player> players = League.KHL.getPlayers();
    private Set<Player> teamRoster;

    @Override
    public void display() {
        System.out.print("Insert the club..");
        try {
            Club team = Club.valueOf(MenuItem.SCAN.nextLine().toUpperCase());
            System.out.println("Sorted by age roster of a club " + team);
            teamRoster = new TreeSet<>(new AgeComparator());
            ClubRosterDisplay crd = new ClubRosterDisplay(players, teamRoster, team);
            crd.sortedClubRosterDisplay();
        } catch (IllegalArgumentException e){
            System.out.println("No club with such name exists the League!");
        }
    }
}