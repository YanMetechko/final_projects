package by.minsk.itacademy.KHL.menu.menuparts.enquiries;

import by.minsk.itacademy.KHL.domain.League;
import by.minsk.itacademy.KHL.domain.Player;
import by.minsk.itacademy.KHL.menu.menuparts.ResultDisplay;
import java.util.List;

public class AverageGamesPlayed implements ResultDisplay {
    @Override
    public void display() {
        List<Player> list = League.KHL.getPlayers();
        int gamesPlayed = 0;
        int i = 0;
        for (Player player : list) {
            gamesPlayed += player.getMatches();
            i++;
        }
        int averageGamesPlayed = gamesPlayed / i;
        for (Player player : list) {
            if (player.getMatches() >= (averageGamesPlayed - 50) & player.getMatches() <= (averageGamesPlayed + 50)) {
                System.out.println(player);
                System.out.println(player.getMatches() + " games played in the League;");
            }
        }
    }
}
