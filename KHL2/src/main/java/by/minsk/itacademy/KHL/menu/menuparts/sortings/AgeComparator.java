package by.minsk.itacademy.KHL.menu.menuparts.sortings;

import by.minsk.itacademy.KHL.domain.Player;

import java.util.Comparator;

public class AgeComparator implements Comparator<Player> {
    @Override
    public int compare(Player o1, Player o2) {
        return -(o1.getBirthDay().compareTo(o2.getBirthDay()));
    }
}
