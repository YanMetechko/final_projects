package by.minsk.itacademy.KHL.menu.menuparts.enquiries;

import by.minsk.itacademy.KHL.domain.Club;
import by.minsk.itacademy.KHL.domain.League;
import by.minsk.itacademy.KHL.domain.Player;
import by.minsk.itacademy.KHL.menu.menuparts.ResultDisplay;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ClubLeagueCitizenshipStructure implements ResultDisplay {

    private Map<String, Integer> citizenshipsPresented() {
        Map<String, Integer> citizenships = new HashMap<>();
        for (Player player : League.KHL.getPlayers()) {
            if (citizenships.containsKey(player.getCitizenship())) {
                citizenships.put(player.getCitizenship(), (citizenships.get(player.getCitizenship()) + 1));
            } else {
                citizenships.put(player.getCitizenship(), 1);
            }
        }

        return citizenships;
    }

    private void summarizing() {
        Iterator<String> it = citizenshipsPresented().keySet().iterator();
        while (it.hasNext()) {
            String key = it.next();
            System.out.println(key.toUpperCase() + "-native players comprise " +
                    String.format("%.2f", (100 * (citizenshipsPresented().get(key) / (double) League.KHL.getPlayers().size()))) + "% share of League roster");
        }
    }

    private Map<Club, int[]> withinClubCitizenshipStructure() {
        Map<Club, int[]> clubMap = new HashMap<>();
        int[] counter = new int[2];
        for (Player player : League.KHL.getPlayers()) {
            if(!clubMap.containsKey(player.getTeam())){
                clubMap.put(player.getTeam(), new int[2]);
            }
            if (!(player.getCitizenship().equals("RU") || player.getCitizenship().equals("BY"))) {
                counter[0] = clubMap.get(player.getTeam())[0] + 1;
                counter[1] = clubMap.get(player.getTeam())[1] + 1;
                clubMap.put(player.getTeam(), counter);
            } else {
                counter = clubMap.get(player.getTeam());
                counter[1] = counter[1] + 1;
                clubMap.put(player.getTeam(), counter);
            }
        }
        return clubMap;
    }


    @Override
    public void display() {
        this.summarizing();
        Map<Club, int[]> clubMap = this.withinClubCitizenshipStructure();
        Iterator<Club> itr = clubMap.keySet().iterator();
        while (itr.hasNext()) {
            Club club = itr.next();
            System.out.println("Share of foreign players in the roster of " + club + " is " + String.format("%.2f", 100*clubMap.get(club)[0] / (double) clubMap.get(club)[1]) + " %;");
        }
    }
}

