package by.minsk.itacademy.KHL;

import by.minsk.itacademy.KHL.menu.RootMenu;

public class App {
    public static void main(String[] args) {
        new RootMenu().startMenu();
    }
}
