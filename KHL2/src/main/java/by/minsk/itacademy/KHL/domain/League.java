package by.minsk.itacademy.KHL.domain;

import com.fasterxml.jackson.annotation.JsonAnySetter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class League implements Serializable {

    public League(String title){this.leagueName = title;}
    public static final League KHL = new League("KHL");
    private String leagueName;
    private List<Player> players = new ArrayList<>();

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players.clear();
        this.players = players;
    }
    @JsonAnySetter
    public void addAPlayer(Player player){
        players.add(player);
    }

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    public String getLeagueName(){
        return this.leagueName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof League)) return false;
        League league = (League) o;
        return leagueName.equals(league.leagueName) &&
                players.equals(league.players);
    }

    @Override
    public int hashCode() {
        return Objects.hash(leagueName, players);
    }
}


