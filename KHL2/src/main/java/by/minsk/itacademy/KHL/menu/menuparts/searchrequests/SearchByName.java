package by.minsk.itacademy.KHL.menu.menuparts.searchrequests;

import by.minsk.itacademy.KHL.domain.League;
import by.minsk.itacademy.KHL.domain.Player;
import by.minsk.itacademy.KHL.menu.MenuItem;
import by.minsk.itacademy.KHL.menu.menuparts.ResultDisplay;

public class SearchByName implements ResultDisplay {
    @Override
    public void display() {
        System.out.print("Insert the name of a player, your're looking for..");
        String s = MenuItem.SCAN.nextLine();
        int i = 0;
        for (Player player : League.KHL.getPlayers()) {

            if (player.getName().toLowerCase().contains(s.toLowerCase())) {
                if (i==0) System.out.println("FOUND:");
                System.out.println(player);
                i++;
            }
        }
        if (i == 0) System.out.println("There's no player with such name.");
    }
}
