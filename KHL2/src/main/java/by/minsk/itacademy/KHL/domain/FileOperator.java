package by.minsk.itacademy.KHL.domain;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;

public class FileOperator {

    public static final File khlDb = new File("KHL.json");
    public static final FileOperator  fileOperator = new FileOperator();

    public void restore(File file, League league) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(file, league);
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public League load(File file) {
        League league = new League("default");
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(file);
            String leagueName = root.path("leagueName").asText();
            league.setLeagueName(leagueName);
            JsonNode playersNode = root.path("players");
            for (JsonNode node : playersNode) {
                String name = node.path("name").asText();
                String citizenship = node.path("citizenship").asText();
                String birthday = node.path("birthDay").asText();
                int seasons = node.path("seasons").asInt();
                Club team = Club.valueOf(node.path("team").asText());
                Role role = Role.valueOf(node.path("role").asText());
                int matches = node.path("matches").asInt();
                int effectiveness = node.path("effectiveness").asInt();
                league.addAPlayer(new Player.Builder()
                        .setName(name)
                        .setRole(role)
                        .setBirthDay(birthday)
                        .setTeam(team)
                        .setCitizenship(citizenship)
                        .setSeasons(seasons)
                        .effectiveness(effectiveness)
                        .setMatches(matches)
                        .build());
            }
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return league;
    }
}
