package by.minsk.itacademy.KHL.menu.menuparts.sortings;

import by.minsk.itacademy.KHL.domain.Club;
import by.minsk.itacademy.KHL.domain.Player;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ClubRosterDisplay {
    private List<Player> players;
    private Set<Player> teamRoster;
    private Club team;

    public ClubRosterDisplay(List<Player> players, Set<Player> teamRoster, Club team) {
        this.players = players;
        this.teamRoster = teamRoster;
        this.team = team;
    }

    public void sortedClubRosterDisplay() {
        for (Player player : players) {
            if (player.getTeam() == team) {
                teamRoster.add(player);
            }
        }
        Iterator<Player> it = teamRoster.iterator();
        try {
            while (it.hasNext()) {
                ObjectMapper mapper = new ObjectMapper();
                System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(it.next()));
            }
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
