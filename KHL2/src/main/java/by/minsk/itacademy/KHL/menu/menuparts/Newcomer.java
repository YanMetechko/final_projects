package by.minsk.itacademy.KHL.menu.menuparts;

import by.minsk.itacademy.KHL.domain.*;
import by.minsk.itacademy.KHL.menu.MenuItem;

public class Newcomer implements ResultDisplay {

    private Player player;

    private void include(){
        System.out.print("Input Player's name: ");
        String name = MenuItem.SCAN.nextLine();
        System.out.print("Input Player's role: ");
        Role role = Role.valueOf(MenuItem.SCAN.nextLine());
        System.out.print("Input Player's birthday ('dd.MM.yyyy'): ");
        String birthday = MenuItem.SCAN.nextLine();
        System.out.print("Input Player's club:");
        Club team = Club.valueOf(MenuItem.SCAN.nextLine());
        System.out.print("Input Player's citizenship: ");
        String citizenship = MenuItem.SCAN.nextLine().toUpperCase();
        System.out.print("Input Player's seasons: ");
        int seasons  = MenuItem.SCAN.nextInt();
        MenuItem.SCAN.nextLine();
        System.out.print("Input Player's effectiveness: ");
        int effectiveness  = MenuItem.SCAN.nextInt();
        MenuItem.SCAN.nextLine();
        System.out.print("Input Player's games played in current season: ");
        int matches  = MenuItem.SCAN.nextInt();
        MenuItem.SCAN.nextLine();

        player = new Player.Builder()
                .setName(name)
                .setRole(role)
                .setBirthDay(birthday)
                .setTeam(team)
                .setCitizenship(citizenship)
                .setSeasons(seasons)
                .effectiveness(effectiveness)
                .setMatches(matches)
                .build();
    }

    @Override
    public void display(){
        this.include();
        League.KHL.addAPlayer(player);
        System.out.println("New Player added: "+player);
        FileOperator.fileOperator.restore(FileOperator.khlDb, League.KHL);
    }

}
