package by.minsk.itacademy.KHL.menu.menuparts;

public interface ResultDisplay {
    public void display();
}