package by.minsk.itacademy.KHL.menu.menuparts.enquiries;

import by.minsk.itacademy.KHL.domain.League;
import by.minsk.itacademy.KHL.domain.Player;
import by.minsk.itacademy.KHL.domain.Role;
import by.minsk.itacademy.KHL.menu.menuparts.ResultDisplay;

import java.util.ArrayList;
import java.util.List;

public class RoleStatsHighlight implements ResultDisplay {
    private int minStat;
    private int maxStat;
    private int averageStat;
    private List<Player> roleStaff;

    private void setParams(Role role) {
        roleStaff = new ArrayList<>();
        for (Player player : League.KHL.getPlayers()) {
            if (player.getRole().equals(role)) {
                if (roleStaff.size() == 0) {
                    maxStat = player.getEffectiveness();
                    minStat = maxStat;
                }
                roleStaff.add(player);
                averageStat += player.getEffectiveness();
                if (player.getEffectiveness() > maxStat) maxStat = player.getEffectiveness();
                if (player.getEffectiveness() < minStat) minStat = player.getEffectiveness();
            }
        }
        averageStat = averageStat / roleStaff.size();
        System.out.println("Statistics among " + role);
    }


    @Override
    public void display() {
        for (Role role : Role.values()) {
            this.setParams(role);
            StringBuilder leaderList = new StringBuilder();
            StringBuilder trailList = new StringBuilder();
            StringBuilder averageList = new StringBuilder();
            for (Player player : roleStaff) {
                if (player.getEffectiveness() >= (maxStat - 2)) leaderList.append(player.getName()).append(", ")
                        .append(player.getTeam()).append(", scored ").append(player.getEffectiveness()).append("\n");
                else if (player.getEffectiveness() <= (minStat + 2)) trailList.append(player.getName()).append(", ")
                        .append(player.getTeam()).append(", scored ").append(player.getEffectiveness()).append("\n");
                else if (player.getEffectiveness() >= (averageStat - 3) & player.getEffectiveness() <= (averageStat + 3))
                    averageList.append(player.getName()).append(", ")
                            .append(player.getTeam()).append(", scored ").append(player.getEffectiveness()).append("\n");
            }

            System.out.println("League scoring leaders are: " + "\n" + leaderList.toString());
            System.out.println("League poor scorers are:" + "\n" + trailList.toString());
            System.out.println("League average scorers are:" + "\n" + averageList.toString());
        }
    }
}

