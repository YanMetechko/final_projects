package by.minsk.itacademy.KHL.menu;

import by.minsk.itacademy.KHL.menu.menuparts.Newcomer;
import by.minsk.itacademy.KHL.menu.menuparts.ResultDisplay;
import by.minsk.itacademy.KHL.menu.menuparts.enquiries.AverageGamesPlayed;
import by.minsk.itacademy.KHL.menu.menuparts.enquiries.ClubLeagueCitizenshipStructure;
import by.minsk.itacademy.KHL.menu.menuparts.enquiries.RoleStatsHighlight;
import by.minsk.itacademy.KHL.menu.menuparts.searchrequests.SearchByName;
import by.minsk.itacademy.KHL.menu.menuparts.searchrequests.SearchByRole;
import by.minsk.itacademy.KHL.menu.menuparts.sortings.SortedByAgeClubResultDisplay;
import by.minsk.itacademy.KHL.menu.menuparts.sortings.SortedBySeasonsClubResultDisplay;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MenuItem {

    private Map<Integer, ResultDisplay> commands = new HashMap<>();
    public static final Scanner SCAN = new Scanner(System.in);

    public MenuItem() {
        this.commands.put(1, new SortedByAgeClubResultDisplay());
        this.commands.put(2, new SortedBySeasonsClubResultDisplay());
        this.commands.put(3, new SearchByName());
        this.commands.put(4, new SearchByRole());
        this.commands.put(5, new AverageGamesPlayed());
        this.commands.put(6, new RoleStatsHighlight());
        this.commands.put(7, new ClubLeagueCitizenshipStructure());
        this.commands.put(8, new Newcomer());
    }

    public void menuDisplay(){
        System.out.println();
        System.out.println("Menu\n(Type the number of a desired command)\n1. Sorting by Age\n2. Sorting by Seasons\n" +
                "3. Search by name\n4. Search by Role\n5. AGP Player list\n6. Stats Highlights\n"+
                "7. Citizenship structure\n8. New player addition");
        int i = SCAN.nextInt();
        SCAN.nextLine();
        this.commands.get(i).display();
    }
}


