package by.minsk.itacademy.KHL.menu.menuparts.searchrequests;

import by.minsk.itacademy.KHL.domain.League;
import by.minsk.itacademy.KHL.domain.Player;
import by.minsk.itacademy.KHL.menu.MenuItem;
import by.minsk.itacademy.KHL.menu.menuparts.ResultDisplay;

public class SearchByRole implements ResultDisplay {
    @Override
    public void display() {
        System.out.print("Insert the Role of a player, your're looking for..");
        String s = MenuItem.SCAN.nextLine();
        for (Player player : League.KHL.getPlayers()) {
            if (player.getRole().toString().contains(s.toUpperCase())) {
                System.out.println(player);
            }
        }
    }
}
