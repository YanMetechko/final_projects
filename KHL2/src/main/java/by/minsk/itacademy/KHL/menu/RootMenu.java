package by.minsk.itacademy.KHL.menu;

import by.minsk.itacademy.KHL.domain.FileOperator;
import by.minsk.itacademy.KHL.domain.League;

public class RootMenu {

    public void startMenu() {
        League league = FileOperator.fileOperator.load(FileOperator.khlDb);
        League.KHL.setLeagueName(league.getLeagueName());
        League.KHL.setPlayers(league.getPlayers());
        String input = " ";
        MenuItem menu = new MenuItem();
        while (input.equals(" ")) {
            menu.menuDisplay();
            System.out.print("\nInput SPACE to continue or anything else to abort..");
            input = MenuItem.SCAN.nextLine();
        }
    }
}
