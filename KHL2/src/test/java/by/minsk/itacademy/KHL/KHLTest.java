package by.minsk.itacademy.KHL;

import by.minsk.itacademy.KHL.domain.FileOperator;
import by.minsk.itacademy.KHL.domain.League;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class KHLTest
{

    @Test
    public void shouldBeSame(){
        League baseLeague = FileOperator.fileOperator.load(FileOperator.khlDb);
        League league = new League(baseLeague.getLeagueName());
        league.setPlayers(baseLeague.getPlayers());
        File file = new File("test.json");
        FileOperator.fileOperator.restore(file,league);
        league = FileOperator.fileOperator.load(file);
        Assert.assertEquals(league,baseLeague);

    }
}
